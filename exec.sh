#!/bin/bash

sleep 2

rm -rf "$2"
mv "$1" "$2"

dir=$(dirname $0)
rm $dir/exec.bat -f
rm $dir/exec.sh -f
