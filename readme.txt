Bienvenue sur le jeu SmashBall (alias SmashBrocoli)

Lien de téléchargement : https://bitbucket.org/duvalbon1u/release-jeu/get/master.zip

Le jeu requiert Java
Le jeu se lance en ouvrant client.jar avec Java Runtime Environnement (Ouvrir avec > Java Runtime) (les noms peuvent différer)

Principe du jeu :
-----------------

Pour l'instant une partie est infinie, le but est donc de faire le maximum de points
On peut jouer en local à 2 ou 3 joueurs (cliquez sur "2 Joueurs" ou "3 Joueurs") ou en ligne (cliquez sur "Online")

Précisions :
------------

Pour lancer le jeu lancer client.jar
Pour le jeu en ligne on peut jouer sur le réseau local, ce qui necessite que le programme server.jar tourne sur un ordinateur du réseau local, ou en ligne (pas de serveurs encore présents, mais ils peuvent êtres ajoutés dans la configuration)
Pour changer la configuration, cliquez sur la roue en bas à droite du menu, ou éditez le fichier config.ini. Attention, le fichier config.ini est écrasé à chaque changement de configuration dans le menu.

Si le jeu ne se lance pas, essayez d'enlever le mode plein écran (remplacer Fullscreen=true par Fullscreen=false dans config.ini).
S'il marche comme ça, activer le mode fenetré pour jouer en plein écran (Fullscreen=true et WindowedFullscreen=true dans config.ini).
S'il ne marche toujours pas, vérifier que vous avez bien le Java Runtime Environnement version 8 ou plus installé sur votre ordinateur.

Mise à jour :
-------------

Si la valeur de configuration permet la mise à jour automatique (Menu principal > Roue dentée > Chercher les mises à jour au démarrage), le client et le serveur cherchent les mise à jour quand ils sont démarrés et installe la mise à jour si elle est disponible
Le lancement manuel de updater.jar permet aussi de rechercher et installes des mises à jour.
Si une erreur survient, vous pouvez toujours supprimer tous les fichiers du jeu et le retélécharger : https://bitbucket.org/duvalbon1u/release-jeu/get/master.zip

Commandes :
-----------

Les commandes par défaut sont:
	Joueur 1 : ZQSD pour le déplacement, Espace pour tirer, SHIFT pour sprinter
	Joueur 2 : Flèches pour le déplacement, Entrée pour tirer, CTRL pour sprinter
	Joueur 3 : IJKL pour le déplacement, U pour tirer, H pour sprinter

Les commandes sont paramétrables dans le menu de configuration (cliquez sur "Configurer les touches").
Pour changer une valeur, cliquez sur la touche voulue (un texte "appuyez sur une touche" apparaîtra), appuyez sur la touche voulue puis cliquez sur une autre touche ou sur Retour et la touche sera sauvegardée
Attention : aucune vérification n'est faite donc une touche peut faire plusieurs actions

Armes :
--------
Pistolet (arme du début), lance-grenade, lance-roquette, lance-missile télédirigés, mitraillette

On peut charger un tir (pour le pistolet : plus de puissance, pour le lance-grenade et lance-roquette : plus de délai avant explosion)
Les grenades sont en rouge, les balles en blanc

On peut ramasser de nouvelles armes en les touchant, on peut aussi récolter des buffs (vitesse, invincibilité) de cette manière

Pour joueur en ligne, il faut sélectionner un serveur puis sélectionner une salle existante ou créer une nouvelle salle.
Saisir le mot de passe de la salle si nécessaire.
